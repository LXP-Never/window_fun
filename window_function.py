# -*- coding:utf-8 -*-
# Author:凌逆战 | Never.Ling
# Date: 2022/6/16
"""
总结一些语音的窗函数
大部分来自于 numpy\scipy.signal.windows.hamming()
"""

import librosa
import matplotlib.pyplot as plt
import numpy as np
import scipy.signal as signal


def Rectangle_windows(win_length):
    return np.ones((win_length))


def Voibis_windows(win_length):
    """ Voibis_windows窗函数，RNNoise使用的是它，它满足Princen-Bradley准则。
    :param x:
    :param win_length: 窗长
    :return:
    """
    x = np.arange(0, win_length)
    return np.sin((np.pi / 2) * np.sin((np.pi * x) / win_length) ** 2)


# Orka前馈窗
def Orka_forward_window(N1=64, N2=448, hop_size=64, window_len=512):
    analysisWindow = np.zeros(window_len)
    for n in range(window_len):
        if n < N1:
            analysisWindow[n] = np.sin(n * np.pi / (2 * N1)) ** 2
        elif N1 <= n <= N2:
            analysisWindow[n] = 1
        elif N2 < n <= N2 + hop_size:
            analysisWindow[n] = np.sin(np.pi * (N2 + hop_size - n) / (2 * hop_size))

    return analysisWindow


# Orka反馈窗
def Orka_backward_window(N1=64, N2=448, hop_size=64, window_len=512):
    synthesisWindow = np.zeros(window_len)
    for n in range(window_len):
        if n < N2 - hop_size:
            synthesisWindow[n] = 0
        elif N2 - hop_size <= n <= N2:
            synthesisWindow[n] = np.cos(np.pi * (n - N2) / (2 * hop_size)) ** 2
        elif N2 < n <= N2 + hop_size:
            synthesisWindow[n] = np.sin(np.pi * (N2 + hop_size - n) / (2 * hop_size))
    return synthesisWindow
